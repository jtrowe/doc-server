
MAKEFLAGS += --no-builtin-rules
MAKEFLAGS += --warn-undefined-variables

.DELETE_ON_ERROR:


build_dir=build
source_dir=src
doc_dir=$(build_dir)/doc



docs : \
$(doc_dir)/index.html \
$(doc_dir)/asciidoc/userguide.html


$(doc_dir)/index.html : $(source_dir)/index.adoc
	@ mkdir --parents $$(dirname $@)
	asciidoc --backen html5 --out-file $@ $<


.PHONEY : server
server : $(doc_dir)/index.html
	docker run --name doc-server --publish 8080:80 --rm --volume $$PWD/build/doc:/usr/share/nginx/html:ro nginx


#
# AsciiDoc
#


$(doc_dir)/asciidoc/userguide.html : \
/usr/share/doc/asciidoc/userguide.html
	@ mkdir --parents $$(dirname $@)
	cp --archive $< $@


